//
//  HViewController.swift
//  Testt
//
//  Created by MBA0176 on 10/16/19.
//  Copyright © 2019 MBA0176. All rights reserved.
//
import UIKit



class HViewController: UIViewController {
    
    //var animalModel = AnimalModel()

    var listAnimal: AnimalModel?
    var listAnimalItems : AnimalDetail?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        title = "List View Animal"
        let image = UIImage(named: "bgheader")
        navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        
        // button bên trái
        let gridViewButton = UIButton(frame: CGRect(x: 00, y: 20, width: 40, height: 40))
        gridViewButton.addTarget(self, action: #selector(changeView), for: .touchUpInside)
        gridViewButton.setImage(UIImage(named: "gridview"), for: .normal)
        
        let leftCustomView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        leftCustomView.bounds = leftCustomView.bounds.offsetBy(dx: 10, dy: 16)
        leftCustomView.addSubview(gridViewButton)
        
        // button bên phải
        let addNewViewButton = UIButton(frame: CGRect(x: 20, y: 30, width: 100, height: 20))
        addNewViewButton.addTarget(self, action: #selector(changeViewToAddNew), for: .touchUpInside)
        addNewViewButton.setTitle("Thêm mới", for: .normal)
        
        let rightCustomView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        rightCustomView.bounds = rightCustomView.bounds.offsetBy(dx: 10, dy: 16)
        rightCustomView.addSubview(addNewViewButton)
        
        //config navigate
        let leftBarButton = UIBarButtonItem(customView: leftCustomView)
        navigationItem.leftBarButtonItem = leftBarButton
        let rightBarButton = UIBarButtonItem(customView: rightCustomView)
        navigationItem.rightBarButtonItem = rightBarButton
        
        //config tableView
        configTable()
        getData()
        
    }
    
    
    func getData(){
        let urlString = URL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1")
        if let url = urlString {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error ?? "nil")
                } else {
                    if let data = data {
                        do {
                            let decoder = JSONDecoder()
                            self.listAnimal = try decoder.decode(AnimalModel.self, from: data)
                            OperationQueue.main.addOperation {
                                //self.refreshControl.endRefreshing()
                                self.tableView.reloadData()
                            }
                        } catch { print(error) }
                    }
                }
            }
            task.resume()
        }
    }
    
    @objc func changeView(){
        let gridViewController = GridViewController()
        navigationController?.pushViewController(gridViewController, animated: true)
    }
    
    @objc func changeViewToAddNew(){
        let AddNewViewController = addNewViewController()
        navigationController?.pushViewController(AddNewViewController, animated: true)
    }
    
    func configTable(){
        let nib = UINib(nibName: "AnimalTableViewCell", bundle: Bundle.main)
        tableView.register(nib, forCellReuseIdentifier: "AnimalTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }

}

extension HViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let animalItem = self.listAnimal{
            return animalItem.items.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnimalTableViewCell", for: indexPath) as! AnimalTableViewCell
        if let animalItem = self.listAnimal{
            let mediaM = "photo-1503066211613-c17ebc9daef0"
            cell.updataTableCell(img: mediaM, title: animalItem.items[indexPath.row].title, mail: animalItem.items[indexPath.row].author)
        }
        return cell
    }
}

extension HViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let animalDetaiViewController = AnimalDetailViewController()
        if let animalItem = self.listAnimal{
            animalDetaiViewController.AnimalName  = animalItem.items[indexPath.row].title
            animalDetaiViewController.AnimalContent  = animalItem.items[indexPath.row].description
        }
        animalDetaiViewController.AnimalImage = "photo-1503066211613-c17ebc9daef0"
        navigationController?.pushViewController(animalDetaiViewController, animated: true)
    }
}
