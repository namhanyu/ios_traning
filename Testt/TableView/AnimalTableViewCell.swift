//
//  AnimalTableViewCell.swift
//  Testt
//
//  Created by DN3-NAMPH on 10/17/19.
//  Copyright © 2019 MBA0176. All rights reserved.
//

import UIKit

class AnimalTableViewCell: UITableViewCell {

    @IBOutlet weak var imgAnimal: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMail: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updataTableCell(img: String, title: String, mail: String){
        imgAnimal.image = UIImage(named: img)
        lblTitle.text = title
        lblMail.text = mail
    }
    
}
