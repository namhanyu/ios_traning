//
//  ListAnimalCollectionViewCell.swift
//  Testt
//
//  Created by DN3-NAMPH on 10/17/19.
//  Copyright © 2019 MBA0176. All rights reserved.
//

import UIKit

class ListAnimalCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgAnimal: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateCell(img: String, title: String, mail: String){
        imgAnimal.image = UIImage(named: img)
        lblTitle.text = title
        lblMail.text = mail
    }
}
