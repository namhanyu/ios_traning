//
//  GridViewController.swift
//  Testt
//
//  Created by DN3-NAMPH on 10/16/19.
//  Copyright © 2019 MBA0176. All rights reserved.
//

import UIKit

class GridViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var listAnimal: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Grid view Animal"
        
        // button bên trái
        let gridViewButton = UIButton(frame: CGRect(x: 00, y: 20, width: 40, height: 40))
        gridViewButton.addTarget(self, action: #selector(changeView), for: .touchUpInside)
        gridViewButton.setImage(UIImage(named: "listView"), for: .normal)
        
        let leftCustomView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        leftCustomView.bounds = leftCustomView.bounds.offsetBy(dx: 10, dy: 16)
        leftCustomView.addSubview(gridViewButton)
        
        // button bên phải
        let addNewViewButton = UIButton(frame: CGRect(x: 20, y: 30, width: 100, height: 20))
        addNewViewButton.addTarget(self, action: #selector(changeViewToAddNew), for: .touchUpInside)
        addNewViewButton.setTitle("Thêm mới", for: .normal)
        
        let rightCustomView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        rightCustomView.bounds = rightCustomView.bounds.offsetBy(dx: 10, dy: 16)
        rightCustomView.addSubview(addNewViewButton)
        
        //config
        let leftBarButton = UIBarButtonItem(customView: leftCustomView)
        navigationItem.leftBarButtonItem = leftBarButton
        let rightBarButton = UIBarButtonItem(customView: rightCustomView)
        navigationItem.rightBarButtonItem = rightBarButton
        
        loadData()
        configCollectionView()
    }


    @objc func changeView(){
        let hViewController = HViewController()
        navigationController?.pushViewController(hViewController, animated: true)
    }
    
    @objc func changeViewToAddNew(){
        let AddNewViewController = addNewViewController()
        navigationController?.pushViewController(AddNewViewController, animated: true)
    }
    
    func configCollectionView(){
        let celNib = UINib(nibName: "ListAnimalCollectionViewCell", bundle: Bundle.main)
        collectionView.register(celNib, forCellWithReuseIdentifier: "ListAnimalCollectionViewCell")
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func loadData(){
        guard let path = Bundle.main.url(forResource: "ListAnimal", withExtension: "plist")
            else { return }
        guard let ListAnimalData = NSArray(contentsOf: path) as? [String] else {
            return
        }
        listAnimal = ListAnimalData
    }

}

extension GridViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return listAnimal.count
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListAnimalCollectionViewCell", for: indexPath) as! ListAnimalCollectionViewCell
        cell.updateCell(img: "photo-1503066211613-c17ebc9daef0", title: "Con cọp", mail: "nobody@flickr.com")
        return cell
    }
    
}

extension GridViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let animalDetaiViewController = AnimalDetailViewController()
        animalDetaiViewController.AnimalName = listAnimal[indexPath.row]
        animalDetaiViewController.AnimalImage = "photo-1503066211613-c17ebc9daef0"
        animalDetaiViewController.AnimalContent = "Đây là thông tin chi tiết của Animal."
        navigationController?.pushViewController(animalDetaiViewController, animated: true)
    }
}
