//
//  File.swift
//  Testt
//
//  Created by DN3-NAMPH on 10/18/19.
//  Copyright © 2019 MBA0176. All rights reserved.
//

import Foundation




struct AnimalModel:Codable {
    var title: String
    var link: String
    var description: String
    var modified: String
    var generator : String
    var items : [AnimalDetail]
    
}
struct AnimalDetail: Codable {
    var title : String
    var link: String
    var media: mediaDetail
    var date_taken: String
    var description: String
    var published: String
    var author: String
    var author_id: String
    var tags: String
}

struct mediaDetail : Codable{
    var m: String
}
