//
//  AnimalDetailViewController.swift
//  Testt
//
//  Created by DN3-NAMPH on 10/17/19.
//  Copyright © 2019 MBA0176. All rights reserved.
//

import UIKit

class AnimalDetailViewController: UIViewController {

    var AnimalName = ""
    var AnimalImage = ""
    var AnimalContent = ""
    
    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = AnimalName
        imgDetail.image = UIImage(named: AnimalImage)
        lblContent.text = AnimalContent
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



