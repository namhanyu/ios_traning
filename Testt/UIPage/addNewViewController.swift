//
//  addNewViewController.swift
//  Testt
//
//  Created by DN3-NAMPH on 10/16/19.
//  Copyright © 2019 MBA0176. All rights reserved.
//

import UIKit

class addNewViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Add new animal"
        // button bên phải
        let addNewViewButton = UIButton(frame: CGRect(x: 45, y: 30, width: 100, height: 20))
        addNewViewButton.addTarget(self, action: #selector(saveAnimal), for: .touchUpInside)
        addNewViewButton.setTitle("Lưu", for: .normal)
        
        let rightCustomView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        rightCustomView.bounds = rightCustomView.bounds.offsetBy(dx: 10, dy: 16)
        rightCustomView.addSubview(addNewViewButton)
        
        let rightBarButton = UIBarButtonItem(customView: rightCustomView)
        navigationItem.rightBarButtonItem = rightBarButton
    }

    @objc func saveAnimal(){
        print("saved new animal")
        //let listAnimalViewController = HViewController()
        //navigationController?.pushViewController(listAnimalViewController, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
